FROM image-mirror-prod-registry.cloud.duke.edu/centos/httpd-24-centos7:latest

LABEL name 'duke-modshib'
LABEL version 'centos-apache'
LABEL release '1.0.0'
LABEL vendor 'Duke University, Office of Information Technology, Automation'
LABEL URL 'https://cloud.duke.edu'
LABEL architecture 'x86_64'
LABEL summary 'OIT Systems Apache with Shibboleth image'
LABEL description 'OIT Systems Apache with Shibboleth image'
LABEL distribution-scope 'public'
LABEL authoritative-source-url 'https://gitlab.oit.duke.edu/devil_ops/duke-shibboleth-contianer'
LABEL changelog-url 'https://gitlab.oit.duke.edu/devil_ops/duke-shibboleth-contianer/activity'

# twistlock scan prevention
LABEL twistlockenv=production

ARG CI_COMMIT_SHA=unspecified
LABEL git_commit=${CI_COMMIT_SHA}

ARG CI_PROJECT_URL=unspecified
LABEL git_repository_url=${CI_PROJECT_URL}

# Switch back to root
USER 0

# Install shib and update vulnerable packages
COPY shibboleth-oit.repo /etc/yum.repos.d/shibboleth-oit.repo
RUN yum install -y --nogpgcheck shibboleth \
    bind-libs \
    bind-license \
    bind-utils \
    libssh2 \
    python \
    python-libs \
      && yum update --nogpgcheck -y \
      && yum clean all \
      && rm -rf /var/cache/yum

# Workaround for CVE-2020-1934
# TODO: Remove once the SCL version of httpd24 addresses
#         this CVE
RUN sed -ie '/LoadModule proxy_ftp_module/d' \
      /opt/rh/httpd24/root/etc/httpd/conf.modules.d/00-proxy.conf

# Copied from upstream
# https://github.com/sclorg/httpd-container/tree/master/2.4
ADD index.html /tmp/src/index.html
RUN chown -R 1001:0 /tmp/src

# Copied from upstream
# https://github.com/sclorg/httpd-container/blob/master/2.4/Dockerfile
RUN /usr/libexec/httpd-prepare && rpm-file-permissions && rm -f /var/www && mkdir -p /var/www/html

# Switch back to an unprivileged user
USER 1001

CMD run-httpd
