Duke Shibboleth
=========================

This Community Supported Resource contains resources to create and
deploy Apache and Shibboleth into a Kubernetes Cluster. It is offered
as part of the [Duke Openshift Users Group Community Supported Resources](https://duke_openshift_users.pages.oit.duke.edu/community-supported-resources/guide).

### Docker Images

#### Architecture

*  Designed to run *just* the Shibboleth daemon in one container, and *just* the
   Apache + mod_shib daemon in another container
*  Expects Shibboleth container and Apache container to share the Shibboleth socket
*  Each container must run as a non-root user with an arbitrary userid
   (eg: as would be in OpenShift)
*  Each contianer Logs to stdout

#### Docker Images

Supports Dockerfiles to create two Images:

- Dockerfile-duke-httpd-modshib: Creates a Docker Image to run the
  Apache webserver, with mod_shib installed and activated
- Dockerfile-duke-shibboleth: Creates a Docker Image to run the
  shibboleth daemon

#### Running with Docker/Docker-Compose

Running these containers with Docker or Docker Compose requires a few volume
mounts from the host:

*   A volume mount for any config file you want to customize in /etc/shibboleth
or /etc/httpd/conf.d
*   Volume mounts for each SP TLS key and certificate
*   A volume mount for /var/run/shibboleth/shibd.sock, shared with the Apache
container with mod_shib installed.

An example Docker Compose config might resemble:

```
# Docker Compose
services:
  shibd:
    image: duke-shibboleth
    volumes:
      - shibboleth2.xml:/etc/shibboleth/shibboleth2.xml:ro
      - attribute-map.xml:/etc/shibboleth/attribute-map.xml:ro
      - sp.key:/etc/pki/tls/private/sp.key:ro
      - sp.crt:/etc/pki/tls/certs/sp.crt:ro
      - shibd.sock:/var/run/shibboleth/shib.sock
  httpd:
    image: duke-httpd-modshib
    ports:
      - 8080:8080
    volumes:
      - shibboleth2.xml:/etc/shibboleth/shibboleth2.xml:ro
      - shibd.sock:/var/run/shibboleth/shib.sock
      - application.conf:/etc/httpd/conf.d/application.conf
      - servername.conf:/etc/httpd/conf.d/servername.conf
```

##### Creating the Shibboleth Response Encryption Certificate

Shibboleth requires each application being protected to have
a self-signed encryption certificate and key. The certificate must be registered with
the [Duke shibboleth registry management system](https://authentication.oit.duke.edu/manager/) along with the url to the apache
webserver/proxy as its Entity ID. The certificate must be created with the host
of this same url as its primary CN so that it matches the Entity ID registered
with the shibboleth administration system. This Entity ID must also be used in
the shibboleth2.xml used to configure both the shibboleth and apache containers
(see below).

bin/generate_shib_certificate will generate this certificate and key,
along with a certificate signing request.

It requires at least one hostname as argument, which will be
used as the primary CN. If multiple hostname arguments are supplied, it will
generate a certificate with the first hostname as primary CN, and the other
hostnames as subject alternative names.

generate_shib_certificate requires the following environment variables:

- ORG: Duke University, or Duke Health Technology Solutions
- ORGUNIT: The Department managing this shibboleth protected
application_image
- EMAIL: should be an email used to contact a person or group
associated with the ORGUNIT.

##### Building and Publishing To Gitlab Repo Registry

bin/ci/build has been created to run in a Gitlab CI process to
build and publish the httpd and shibboleth images to a Gitlab Project
Image Registry.

Run locally by setting the following environment variables:

- CI_REGISTRY: required unless DO_NO_PUBLISH is true. Full url to the
gitlab registry root (used in docker login).
- CI_REGISTRY_USER: required unless DO_NOT_PUBLISH is true. Username used
to login to the gitlab registry and push images.
- CI_REGISTRY_PASSWORD: required unless DO_NOT_PUBLISH is true. Username
used to login to the gitlab registry and push images.
- CI_REGISTRY_IMAGE: required unless DO_NO_PUBLISH is true. Full url to
the gitlab registry root (used in docker login).
- DO_NOT_PUBLISH: If true (default false), do not login and publish the
image to the gitlab ci project image registry
- DRY_RUN: If true (default false), do not run any docker build or publish commands,
but print the commands that would be run.

All of the environment variables with 'CI_' in the name are
[automatically provided](https://docs.gitlab.com/ee/ci/variables/#predefined-environment-variables) when run in a Gitlab CI Runner.

#### Running with Kubernetes

##### Config File mounting with Kubernetes/OpenShift

The normally-documented way of mounting files from a Kubernetes configMap into a volume involves overwritting the directory into which the volume is mounted.  For example, following most documented examples to mount the shibboleth2.xml file into /etc/shibboleth would end up with /etc/shibboleth empty except for the shibboleth2.xml file.

However, if you create a volume for each file you need to mount from the configMap, and mount them with the specific mountPath to the file, specifying the subPath as the file name as well, the files are mounted individually into the directory.  (Yes, it hurt me to type this as much as it hurt you to read it.  See the example below...)

You need a volume for *each* file (the key from the configMap), and a volume mount for each volume.

Example:

```
# Given a configMap named shib-config that contains the keys "shibboleth2.xml" and "attribute-map.xml" - in the containers spec:

containers:
  - volumeMounts:
    - mountPath: /etc/yum/shibboleth2.xml
      name: volume-shib2xml
      subPath: shibboleth2.xml
    - mountPath: /etc/yum/attribute-map.xml
      name: volume-attributemap
      subPath: attribute-map.xml
    volumes:
    - configMap:
        defaultMode: 420
        name: shib-config
      name: volume-shib2xml
    - configMap:
        defaultMode: 420
        name: shib-config
      name: volume-attributemap
```

##### Shibboleth socket consideration for Kubernetes/OpenShift

The shibboleth container needs to run in the same pod as an Apache
container, because it must share the Shibboleth socket as a volume
mount with Apache.  Both containers need to mount /var/run/shibboleth/
shibd.sock (preferably of type [emptyDir](https://kubernetes.io/docs/concepts/storage/volumes/#emptydir) to be able to talk back and forth.

#### Openshift Templates

The openshift directory contains a set of Templates that can be used
or referenced to build and deploy these containers into Openshift.

#### Helm Chart

The helm-chart/duke-shibboleth contains a helm chart to deploy
these containers into Kubernetes or Openshift, and expose them as a
Service and Route.

### Software Lineage

The version of apache httpd used in these resources is from [centos/httpd-24-centos7](https://hub.docker.com/r/centos/httpd-24-centos7/) which is in turn from [sclorg/httpd-container](https://github.com/sclorg/httpd-container) and if you need to research the specific rpms used to construct that you can see them on [pkgs.org httpd24-httpd](https://pkgs.org/download/httpd24-httpd)
